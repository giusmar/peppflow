process MAKE_INDEX {
    echo true

    input:
    path(fasta)

    output:
    tuple path("genome.fa"), path("genome.fa.*"), indexed

    script:
    """
    bwa index $fasta
    """
}

process DOWNLOAD_INDEX {
    echo true

    input:
    path(fasta)
    path(index)

    output:
    tuple path("${params.genome}_genome.fa"), path("${params.genome}_genome.fa.*"), emit: indexed

    script:
    """
    ln -s genome.fa ${params.genome}_genome.fa
    ln -s genome.fa.amb ${params.genome}_genome.fa.amb
    ln -s genome.fa.ann ${params.genome}_genome.fa.ann
    ln -s genome.fa.bwt ${params.genome}_genome.fa.bwa
    ln -s genome.fa.pac ${params.genome}_genome.fa.pac
    ln -s genome.fa.sa ${params.genome}_genome.fa.sa
    """
}