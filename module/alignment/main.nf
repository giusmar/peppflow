process ALIGN {
    echo true
    label 'align'
    tag 'BWA'
    publishDir "$params.outdir" , mode: 'copy',
    saveAs: {filename ->
             if (filename.indexOf("bam") > 0)     "bwa/aligned_bam/$filename"          
        else null            
    }

    input:
    tuple val(sample_id), val(type), val(peak), val(rep), val(exp), path(read)
    path(fasta)
    path(index)

    output:
    tuple val(sample_id), val(type), val(peak), val(rep), val(exp), path("*.bam"), emit: aligned_bam

    script:
    """
    bwa mem -M 
    """

}