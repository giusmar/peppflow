process FASTQC {
    echo true
    label 'fastqc'
    tag 'FASTQC'
    publishDir "$params.outdir" , mode: 'copy',
    saveAs: {filename ->
             if (filename.indexOf("zip") > 0)     "fastqc/zips/$filename"
        else if (filename.indexOf("html") > 0)    "fastqc/$filename"            
        else null            
    }

    input:
    tuple val(sample_id), val(type), val(peak), val(rep), val(exp), path(read)

    output:
    tuple val(sample_id), val(type), val(peak), val(rep), val(exp), path("*.{zip,html}"), emit: fastqc_mqc

    script:
    """
    ln -s ${read} ${sample_id}.fastq.gz
    fastqc ${sample_id}.fastq.gz --quiet
    """
}