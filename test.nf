fasta_ch = Channel.fromPath('gs://tigem-gcacchiarelli-01/Reference/Human_hg38/genome.fa')
gtf_ch   = Channel.fromPath('gs://tigem-gcacchiarelli-01/Reference/Human_hg38/genes.gtf')
index_ch = Channel.fromPath('gs://tigem-gcacchiarelli-01/Reference/Human_hg38/Index/bwa_0.7.17/genome.fa.*', type: 'file')

process takefile {

    input:
    path(index)

    // output:
    // tuple path("${params.genome}_genome.fa"), path("${params.genome}_genome.fa.*"), emit: indexed

    script:
    """
    ln -s genome.fa ${params.genome}_genome.fa
    ln -s genome.fa.amb ${params.genome}_genome.fa.amb
    ln -s genome.fa.ann ${params.genome}_genome.fa.ann
    ln -s genome.fa.bwt ${params.genome}_genome.fa.bwt
    ln -s genome.fa.pac ${params.genome}_genome.fa.pac
    ln -s genome.fa.sa ${params.genome}_genome.fa.sa
    """
}

workflow {
    takefile(index_ch.collect())
}