include { ALIGN } from '../module/alignment/main'

workflow ALIGNMENT {
    take:
    reads

    main:
    ALIGN(reads,fasta_ch.collect(),index_ch.collect())
}