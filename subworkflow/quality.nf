// modules
include { FASTQC } from '../module/fastqc/main'

// workflow
workflow QUALITY { 

    take:
    input_reads_ch
    
    main:
    FASTQC(input_reads_ch)

    emit:
    reads = input_reads_ch
}