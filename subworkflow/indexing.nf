index_ch = Channel.fromPath('gs://tigem-gcacchiarelli-01/Reference/Human_hg38/Index/bwa_0.7.17/genome.fa.{amb,ann,bwt,pac,sa}')

// modules
include { MAKE_INDEX; DOWNLOAD_INDEX } from '../module/index/main'

// workflow
workflow INDEXING {

    take:
    fasta_ch

    main:
    if (!params.downloadIndex) {
        MAKE_INDEX(fasta_ch)

        emit:
        indexed = MAKE_INDEX.out.indexed
    } else {
        DOWNLOAD_INDEX(fasta_ch,index_ch.collect())
        DOWNLOAD_INDEX.out.indexed
    }
}