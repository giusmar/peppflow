// check params
if (params.input)                  { input_ch = file(params.input, checkIfExists: true) }                else { exit 1, 'Input samplesheet not specified!' }

// create channels
input_reads_ch = Channel.fromPath(input_ch)
                            .splitCsv( header:false, sep:',' )
                            .map( { row -> [sample_id = row[0], type = row[1], peak = row[2], rep = row[3], exp = row[4], read = row[5]] } )

if (params.genome){
    switch (params.genome) {
        case 'Human': {
            if (!params.downloadIndex) {
                fasta_ch = Channel.fromPath('gs://tigem-gcacchiarelli-01/Reference/Human_hg38/genome.fa')
                gtf_ch   = Channel.fromPath('gs://tigem-gcacchiarelli-01/Reference/Human_hg38/genes.gtf')
            } else {
                fasta_ch = Channel.fromPath('gs://tigem-gcacchiarelli-01/Reference/Human_hg38/genome.fa')
                gtf_ch   = Channel.fromPath('gs://tigem-gcacchiarelli-01/Reference/Human_hg38/genes.gtf')
                // index_ch = Channel.fromPath('gs://tigem-gcacchiarelli-01/Reference/Human_hg38/Index/bwa_0.7.17/genome.fa.{amb,ann,bwt,pac,sa}')
            }    
        }
    }
}

include { INDEXING } from '../subworkflow/indexing'
include { PREPAREFILES } from '../subworkflow/preparefiles'
include { QUALITY } from '../subworkflow/quality' 
include { ALIGNMENT } from '../subworkflow/alignment' 

workflow CHIPSEQ {
    INDEXING(fasta_ch)
    // QUALITY(input_reads_ch)
    // ALIGNMENT(reads,)
}